# ubuntu-cloud

Unofficial demo and wiki about using Ubuntu in Qemu on GitLab CI

* Server http://cdimage.ubuntu.com/releases/ 715M (torrent) (not sure it can be used as a live CD...)
* Cloud http://cloud-images.ubuntu.com/ 174-323 M (no torrent)
  * i386 version, does not work well with qemu -nographic, but -curses should work. (amd64 version seems OK with -nographic)
  * Image cannot be logged in. It has to be modified.
    * Mounting the image helps for that. (This needs a kernel with nbd ou loop modules, neither of them are available on GitLab CI ~Digital Ocean as of september 2018.)
      ```sh
      sudo qemu-nbd -c /dev/nbd0 bionic-server-cloudimg-amd64.img
      # Mount with file manager
      sudo chroot /media/.../bionic-server-cloudimg-amd64/
      ls /media/.../cloudimg-rootfs/
      # Modify image
      chroot /media/.../cloudimg-rootfs/ # or other strategy
      # Unmount with file manager
      sudo qemu-nbd -d /dev/nbd0
      # ...
      qemu-system-i386 -nographic bionic-server-cloudimg-amd64.img 
      ```
    * Probable official strategy '/etc/cloud/cloud.cfg'
      * [cloud-init](https://cloud-init.io/) ([doc](https://cloudinit.readthedocs.io), [KVM](https://cloudinit.readthedocs.io/en/latest/topics/debugging.html#analyze-quickstart-kvm)) Me be tried.
      * [Generate User Password in Ubuntu Cloud Image](http://www.yamansertdemir.com/?p=55)
        gives some hint on how to use 'cloud.cfg', 'guestfish' is available on [Debian](https://packages.debian.org/search?searchon=contents&keywords=guestfish), not yet on [Alpine](https://pkgs.alpinelinux.org/contents?file=guestfish)
    * Simple strategy
      * [Access a qcow2 Virtual Disk Image From The Host](https://www.jamescoyle.net/how-to/1818-access-a-qcow2-virtual-disk-image-from-the-host)
* Minimal (KVM or LXD) http://cloud-images.ubuntu.com/minimal/releases/ 93-158M (no torrent)
* [Install Ubuntu Core on KVM](https://www.ubuntu.com/download/iot/kvm)
